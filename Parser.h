#ifndef INC_19210_GAYFUTDINOV_LAB2_PARSER_H
#define INC_19210_GAYFUTDINOV_LAB2_PARSER_H
#include "IParser.h"
#include <sstream>

using namespace std;

class Parser: public IParser{
private:
    vector<unsigned int> getSequence(string line) const;
    void addBlock(map<unsigned int, string>* blocks, string line);
public:
    virtual pair< map<unsigned int, string>, vector<unsigned int> > parse(ifstream& wfile);
};
#endif