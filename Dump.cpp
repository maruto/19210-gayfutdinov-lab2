#include "Dump.h"
#include "WorkflowException.h"
#include <fstream>

Dump::Dump(vector<string> arg){
    file = arg[1];
}

vector<string> Dump::execute(vector<string> text){
    ofstream dumpfile(file);
    if (!dumpfile.is_open()) {// если файл не открыт
        dumpfile.close();
        throw WorkflowException("The file for dumping cannot be opened.");
    } else {
        for(int i = 0; i < text.size(); i++)
            dumpfile << text[i] + '\n';
    }
    dumpfile.close();
    return text;
}

Dump::~Dump() = default;