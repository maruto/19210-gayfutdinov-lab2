#ifndef INC_19210_GAYFUTDINOV_LAB2_WORKER_H
#define INC_19210_GAYFUTDINOV_LAB2_WORKER_H
#include <string>
#include <vector>

using namespace std;

class Worker{
public:
    virtual vector<string> execute(vector<string> text) = 0;
    virtual ~Worker(){};
};

#endif