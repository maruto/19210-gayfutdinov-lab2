#ifndef INC_19210_GAYFUTDINOV_LAB2_DUMP_H
#define INC_19210_GAYFUTDINOV_LAB2_DUMP_H
#include "Worker.h"

class Dump: public Worker {
private:
    string file;
public:
    Dump(vector<string> arg);
    virtual vector<string> execute(vector<string> text);
    virtual ~Dump();
};

#endif