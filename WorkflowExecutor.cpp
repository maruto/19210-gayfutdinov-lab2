#include "WorkflowExecutor.h"
#include "Validator.h"
#include "WorkerFactory.h"

void WorkflowExecutor::execute(pair< map<unsigned int, string>, vector<unsigned int> >* schema) const{
    Validator val;
    val.validate(schema);

    vector<unsigned int>& sequence = schema->second;//ссылка на вектор индексов
    int sequenceSize = schema->second.size(); //размер вектора индексов схемы
    map<unsigned int, string>& blocks = schema->first; //ссылка на словарь с блоками
    map<unsigned int, string> :: iterator it = blocks.begin();

    vector<string> text;

    for(int i = 0; i < sequenceSize; i++){
        vector<string> arguments; //вектор параметров для вызова Worker'a
        string tmpStr;
        it = blocks.find(sequence[i]); //находим нужный блок
        istringstream iss(it->second, istringstream::in);
        while(iss >> tmpStr)
            arguments.push_back(tmpStr); //параметры в вектор

        WorkerFactory workerFac; Worker *ptr;
        ptr = workerFac.createWorker(arguments);
        text = ptr->execute(text);
        delete ptr;
    }
};

