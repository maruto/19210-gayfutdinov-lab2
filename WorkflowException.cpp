#include "WorkflowException.h"

WorkflowException::WorkflowException(string error): mError(error){}

string WorkflowException::getError(){
    return mError;
}