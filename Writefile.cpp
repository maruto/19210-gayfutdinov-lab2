#include "Writefile.h"
#include "WorkflowException.h"
#include <fstream>

Writefile::Writefile(vector<string> arg){
    file = arg[1];
}

vector<string> Writefile::execute(vector<string> text){
    ofstream outfile(file);
    if (!outfile.is_open()) {// если файл не открыт
        outfile.close();
        throw WorkflowException("The out file cannot be opened.");
    } else {
        for(int i = 0; i < text.size(); i++)
            outfile << text[i] + '\n';
    }
    outfile.close();
    return text;
};

 Writefile::~Writefile() = default;