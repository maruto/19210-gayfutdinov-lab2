#ifndef INC_19210_GAYFUTDINOV_LAB2_WORKFLOWEXCEPTION_H
#define INC_19210_GAYFUTDINOV_LAB2_WORKFLOWEXCEPTION_H
#include <string>
#include <exception>

using namespace std;

class WorkflowException: public exception {
private:
    string mError;
public:
    explicit WorkflowException(string error);
    string getError();
};
#endif
