#include "WorkerFactory.h"
#include "Readfile.h"
#include "Writefile.h"
#include "Replace.h"
#include "Grep.h"
#include "Sort.h"
#include "Dump.h"

Worker* WorkerFactory::createWorker(vector<string> parametrs) {
    if(parametrs[0] == "readfile" ) {
        Readfile *workerPtr;
        workerPtr = new Readfile(parametrs);
        return workerPtr;
    } else if (parametrs[0] == "writefile") {
        Writefile *workerPtr;
        workerPtr = new Writefile(parametrs);
        return workerPtr;
    }else if (parametrs[0] == "replace") {
        Replace *workerPtr;
        workerPtr = new Replace(parametrs);
        return workerPtr;
    }else if (parametrs[0] == "grep") {
        Grep *workerPtr;
        workerPtr = new Grep(parametrs);
        return workerPtr;
    }else if (parametrs[0] == "sort") {
        Sort *workerPtr;
        workerPtr = new Sort();
        return workerPtr;
    }else if (parametrs[0] == "dump") {
        Dump *workerPtr;
        workerPtr = new Dump(parametrs);
        return workerPtr;
    }
}