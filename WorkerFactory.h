#ifndef INC_19210_GAYFUTDINOV_LAB2_WORKERFACTORY_H
#define INC_19210_GAYFUTDINOV_LAB2_WORKERFACTORY_H
#include "Worker.h"


class WorkerFactory{
public:
    Worker* createWorker(vector<string> parametrs);
};

#endif
