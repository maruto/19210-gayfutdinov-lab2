#ifndef INC_19210_GAYFUTDINOV_LAB2_WRITEFILE_H
#define INC_19210_GAYFUTDINOV_LAB2_WRITEFILE_H
#include "Worker.h"

class Writefile: public Worker {
private:
    string file;
public:
    Writefile(vector<string> arg);
    virtual vector<string> execute(vector<string> text);
    virtual ~Writefile();
};

#endif