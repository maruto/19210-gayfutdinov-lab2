#include "Sort.h"
#include <algorithm>

vector<string> Sort::execute(vector<string> text){
    sort(text.begin(), text.end());
    return text;
};

Sort::~Sort() = default;