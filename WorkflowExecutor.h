#ifndef INC_19210_GAYFUTDINOV_LAB2_WORKFLOWEXECUTOR_H
#define INC_19210_GAYFUTDINOV_LAB2_WORKFLOWEXECUTOR_H
#include <string>
#include <map>
#include <vector>

using namespace std;

class WorkflowExecutor{
public:
   void execute(pair< map<unsigned int, string>, vector<unsigned int> >* schema) const;
};
#endif
