/*
 * класс который начинает и заканчивает работу всей схемы
 * */
#ifndef INC_19210_GAYFUTDINOV_LAB2_WORKFLOW_H
#define INC_19210_GAYFUTDINOV_LAB2_WORKFLOW_H
#include <string>
#include <fstream>

using namespace std;

class Workflow {
public:

    void start(string file);
};


#endif
