#include "Workflow.h"

int main(int argc, char* argv[]) {
    Workflow prog;
    prog.start(argv[1]);

    return 0;
}
