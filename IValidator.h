#ifndef INC_19210_GAYFUTDINOV_LAB2_IVALIDATOR_H
#define INC_19210_GAYFUTDINOV_LAB2_IVALIDATOR_H
#include <map>
#include <vector>
#include <string>
#include <fstream>

using namespace std;

class IValidator {
protected:
    virtual void validate(pair< map<unsigned int, string>, vector<unsigned int> >* schema) const = 0;
};
#endif