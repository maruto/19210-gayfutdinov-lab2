#include "Workflow.h"
#include "WorkflowException.h"
#include "Parser.h"
#include "WorkflowExecutor.h"
#include <iostream>


void Workflow::start(string file){
    ifstream wfile(file);
    try {
        if (!wfile.is_open())// если файл не открыт
            throw WorkflowException("The workflow file cannot be opened.");
        else {
            Parser par;
            pair< map<unsigned int, string>, vector<unsigned int> > schema = par.parse(wfile);
            WorkflowExecutor wfExecutor;
            wfExecutor.execute(&schema);
        }
    }
    catch(WorkflowException &exception) {
        cerr  << "Error: " << exception.getError() << '\n';
    }
    catch (std::exception &exception){
        std::cerr << "Error" << exception.what() << '\n';
    }
    wfile.close();
}