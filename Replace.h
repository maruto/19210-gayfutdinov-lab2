#ifndef INC_19210_GAYFUTDINOV_LAB2_REPLACE_H
#define INC_19210_GAYFUTDINOV_LAB2_REPLACE_H
#include "Worker.h"

class Replace: public Worker {
private:
    string word_1, word_2;
public:
    Replace(vector<string> arg);
    virtual vector<string> execute(vector<string> text);
    virtual ~Replace();
};

#endif