#ifndef INC_19210_GAYFUTDINOV_LAB2_VALIDATOR_H
#define INC_19210_GAYFUTDINOV_LAB2_VALIDATOR_H
#include "IValidator.h"
#include <sstream>

using namespace std;

class Validator: public IValidator{
private:
    pair<string, int> getDate(string parameter) const;
public:
    virtual void validate(pair< map<unsigned int, string>, vector<unsigned int> >* schema) const;
};
#endif
