#include "Grep.h"

Grep::Grep(vector<string> arg){
    word = arg[1];
}

vector<string> Grep::execute(vector<string> text) {
    vector<string> newText;
    for(auto it = begin(text); it != end(text); ++it){
        if(!(strstr(it->c_str(), word.c_str()) == nullptr))
            newText.push_back(*it);
    }
    return newText;
}

Grep::~Grep() = default;