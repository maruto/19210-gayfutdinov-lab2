#ifndef INC_19210_GAYFUTDINOV_LAB2_IPARSER_H
#define INC_19210_GAYFUTDINOV_LAB2_IPARSER_H
#include <map>
#include <vector>
#include <string>
#include <fstream>

using namespace std;

class IParser {
protected:
    virtual pair< map<unsigned int, string>, vector<unsigned int> > parse(ifstream& wfile) = 0;
};

#endif