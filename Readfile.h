#ifndef INC_19210_GAYFUTDINOV_LAB2_READFILE_H
#define INC_19210_GAYFUTDINOV_LAB2_READFILE_H
#include "Worker.h"

class Readfile: public Worker {
private:
    string file;
public:
    Readfile(vector<string> arg);
    virtual vector<string> execute(vector<string> text);
    virtual ~Readfile();
};

#endif
