#include "Parser.h"
#include "WorkflowException.h"

//метод для создания схемы, идём по файлу и записываем нужное в мапу, в конце осотовляем вектор с порядком выполнения блоков
pair< map<unsigned int, string>, vector<unsigned int> > Parser::parse(ifstream& wfile){
    string tempStr = "\0";
    map<unsigned int, string> blocks;
    vector<unsigned int> sequence;
    bool startflag = false;

    while(!getline(wfile, tempStr).eof()) {
        if(tempStr == "desc") {
            startflag = true;
        } else if(tempStr == "csed") {
            startflag = false;
            getline(wfile, tempStr);
            sequence = getSequence(tempStr); //запись вектора значений
        } else if(startflag)
            addBlock(&blocks, tempStr);
    }

    return make_pair(blocks, sequence);
}

//метод для создания вектора значений с порядком выполнения блоков
vector<unsigned int> Parser::getSequence(string line) const{
    istringstream iss(line, istringstream::in); //строку в поток
    vector<unsigned int> out;
    unsigned int K = 0, id;
    string word;
    while(iss >> word) {
        if(!(K%2)){
            id = stoi(word);
            out.push_back(id);
        } else if(word != "->")
            throw WorkflowException("Expected keyword \"->\", instead of \"" + word + "\".\n");
        K++;
    }
    return out;
}

//метод для добавления очередного блока в мапу
void Parser::addBlock(map<unsigned int, string>* blocks, string line){
    istringstream iss(line, istringstream::in); //строку в поток
    string idStr; unsigned int id;
    iss >> idStr; //обрабатываем до " = "
    for(int i = 0;i < idStr.length(); i++)
        if (idStr[i] < '0' || idStr[i] > '9') {
            blocks->clear();
            throw WorkflowException("Invalid index (" + idStr + ") found in command block: " +
                                line);//индексы в блоке с командами записаны некорректно
        }
    id = stoi(idStr);


    string tempStr, parametr = "\0"; //обрабатываем после " = "
    iss >> tempStr;
    while(iss >> tempStr) {
        parametr += tempStr;
        parametr.resize(parametr.size() + 1, ' ');
    }
    parametr.resize(parametr.size() - 1);

    if(blocks->end() ==  blocks->find(id)){
        blocks->insert(pair<unsigned int, string>(id, parametr));
    }else {
        blocks->clear();
        throw WorkflowException("Duplicate index (" + to_string(id) +
                            ") found in command block");//в блоке с командами повторяются индексы
    }
}
