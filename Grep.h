#ifndef INC_19210_GAYFUTDINOV_LAB2_GREP_H
#define INC_19210_GAYFUTDINOV_LAB2_GREP_H
#include "Worker.h"

class Grep: public Worker {
private:
    string word;
public:
    Grep(vector<string> arg);
    virtual vector<string> execute(vector<string> text);
    virtual ~Grep();
};

#endif