#include "Replace.h"

Replace::Replace(vector<string> arg){
    word_1= arg[1];
    word_2 = arg[2];
}

vector<string> Replace::execute(vector<string> text){
    for(auto it = begin(text); it != end(text); it++){
        size_t pos;
        while((pos = it->find(word_1)) != string::npos) {
            it->erase(pos, word_1.size());
            it->insert(pos, word_2);
        }
    }
    return text;
}

Replace::~Replace() = default;