#include "Readfile.h"
#include "WorkflowException.h"
#include <fstream>

Readfile::Readfile(vector<string> arg){
    file = arg[1];
}

vector<string> Readfile::execute(vector<string> text){
    ifstream infile(file);
    if (!infile.is_open()) {// если файл не открыт
        infile.close();
        throw WorkflowException("The out file cannot be opened.");
    } else {
        string tempStr;
        while(!getline(infile, tempStr).eof())
            text.push_back(tempStr);
    }
    infile.close();
    return text;
}

Readfile::~Readfile() = default;