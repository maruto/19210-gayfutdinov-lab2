#ifndef INC_19210_GAYFUTDINOV_LAB2_SORT_H
#define INC_19210_GAYFUTDINOV_LAB2_SORT_H
#include "Worker.h"

class Sort: public Worker {
public:
    virtual vector<string> execute(vector<string> text);
    virtual ~Sort();
};

#endif