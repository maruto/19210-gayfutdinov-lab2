#include "Validator.h"
#include "WorkflowException.h"

/*
(I)-----индексы из вектора должны быть в схеме с блоками
(II)----должны использоваться блоки, котороые определены программой
(III)---блоки содержат нужное количество аругментов
(IV)----первый блок в векторе - это всегда чтение файлов, последний - запись файлов
(V)-----блоки чтения/записи файлов не должны быть в середине схемы
 */
void Validator::validate(pair< map<unsigned int, string>, vector<unsigned int> >* schema) const{
    vector<unsigned int>& sequence = schema->second;//ссылка на вектор индексов
    int sequenceSize = schema->second.size(); //размер вектора индексов схемы
    map<unsigned int, string>& blocks = schema->first; //ссылка на словарь с блоками
    map<unsigned int, string> :: iterator it = blocks.begin();

    // (I)
    for(int i = 0; i < sequenceSize; i++){
        if(blocks.end() ==  blocks.find(sequence[i]))
            throw WorkflowException("Block with given (" + to_string(sequence[i]) + ") index not found");
    }

    //(II) and (III)
    while (it != blocks.end()) {
        pair<string, int> data = getDate(it->second);
        bool flag;
        if(data.first == "readfile" ) {
            flag = (data.second != 1);
        }else if (data.first == "writefile") {
            flag = (data.second != 1);
        }else if (data.first == "grep") {
            flag = (data.second != 1);
        }else if (data.first == "sort") {
            flag = (data.second != 0);
        }else if (data.first == "replace") {
            flag = (data.second != 2);
        }else if (data.first == "dump") {
            flag = (data.second != 1);
        } else //команда не опознана
            throw WorkflowException("Unidentified command at line: " + to_string(it->first) + " = " + it->second);
        if(flag) //если где-либо неверное количество аргументов
            throw WorkflowException("Invalid number of arguments: " + to_string(it->first) + " = " + it->second);
        ++it;
    }

    //(IV) and (V)
    for(int i = 0; i < sequenceSize; i++){
        it = blocks.find(sequence[i]);
        pair<string, int> data = getDate(it->second);
        if(i > 0 && i < (sequenceSize - 1)) {
            if(data.first == "readfile")
                throw WorkflowException("<readfile> cannot be in the middle of the outline. \n");
            else if(data.first == "writefile")
                throw WorkflowException("<writefile> cannot be in the middle of the outline. \n");
        }else if((i == 0) && (data.first != "readfile"))
            throw WorkflowException("The first block must be <readfile> block. Expected  <readfile>. \n");
        else if((i == sequenceSize - 1) && (data.first != "writefile"))
            throw WorkflowException("The last block must be writefile block. Expected  <writefile>. \n");
    }

}

pair<string, int> Validator::getDate(string parameter) const{
    istringstream iss(parameter, istringstream::in);
    string command, argument;
    iss >> command;
    int k = 0;
    while(iss >> argument)
        k++;
    return make_pair(command, k);
}